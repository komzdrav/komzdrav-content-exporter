USE [komzdrav]
GO

CREATE TABLE PersonActivity
(
PersonId uniqueidentifier NOT NULL,
Activity  nvarchar(max)NOT NULL,
)

ALTER TABLE Persons
ADD PRIMARY KEY (Id)

ALTER TABLE PersonActivity
ADD CONSTRAINT fk_PersonActivityPersons
FOREIGN KEY (PersonId)
REFERENCES Persons(Id)