using System.Data;
using System.Data.SqlClient;
using Xunit;

namespace KomzdravContentExporter
{
    public class VirtualEnvironmentTests
    {
        [Fact]
        public void ShouldBeAbleToOpenConnectionToMsSql()
        {
            string connectionString =
                "Server=192.168.10.1;Database=komzdrav;User Id=vagrant;Password=vagrant;";

            var connection = new SqlConnection(connectionString);
            connection.Open();

            Assert.Equal(ConnectionState.Open, connection.State);
        }
    }
}