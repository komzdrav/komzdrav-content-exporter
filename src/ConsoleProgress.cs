﻿using System;
using System.Text;
using System.Threading;

namespace KomzdravContentExporter
{
    public class ConsoleProgress : IProgress<string>, IDisposable
    {
        private readonly string _animation = @"|/-\";
        private readonly TimeSpan _animationInterval = TimeSpan.FromSeconds(1.0/8);
        private int _animationIndex;
        private readonly Timer _timer;
        private bool _disposed;
        private string _currentText = string.Empty;

        public ConsoleProgress()
        {
            _timer = new Timer(TimerHandler);

            if (Console.IsOutputRedirected == false)
            {
                ResetTimer();
            }
        }

        private void ResetTimer()
        {
            _timer.Change(_animationInterval, TimeSpan.FromMilliseconds(-1));
        }

        public void Report(string task)
        {
            Console.Write(task);
        }

        private void TimerHandler(object state)
        {
            lock (_timer)
            {
                if (_disposed)
                {
                    return;
                }
                ShowProgress($"{_animation[_animationIndex++ % _animation.Length]}");

                ResetTimer();
            }
        }

        private void ShowProgress(string text)
        {
            int commonPrefixLength = 0;
            int commonLength = Math.Min(_currentText.Length, text.Length);
            while (commonPrefixLength < commonLength && text[commonPrefixLength] == _currentText[commonPrefixLength])
            {
                commonPrefixLength++;
            }

            // Backtrack to the first differing character
            StringBuilder outputBuilder = new StringBuilder();
            outputBuilder.Append('\b', _currentText.Length - commonPrefixLength);

            // Output new suffix
            outputBuilder.Append(text.Substring(commonPrefixLength));

            // If the new text is shorter than the old one: delete overlapping characters
            int overlapCount = _currentText.Length - text.Length;
            if (overlapCount > 0)
            {
                outputBuilder.Append(' ', overlapCount);
                outputBuilder.Append('\b', overlapCount);
            }

            Console.Write(outputBuilder);
            _currentText = text;

        }

        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }

            lock (_timer)
            {
                _disposed = true;
                ShowProgress(string.Empty);
            }
        }
    }
}