﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using CommandLine;
using MediatR;

namespace KomzdravContentExporter
{
    class Program
    {
        public static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
            var container = builder.Build();
            var mediator = container.Resolve<IMediator>();
            var parser = container.Resolve<Parser>();
            try
            {
                parser.ParseArguments(args,
                    container.Resolve<IEnumerable<IRequest>>().Select(r => r.GetType()).ToArray())
                    .WithNotParsed(e => { if (e.Any()) Environment.Exit(1); })
                    .WithParsed(option => { mediator.Send(option as IRequest); });
            }
            catch (Exception e)
            {
                Console.Error.Write(e.Message);
                Environment.Exit(1);
            }
            finally
            {
                container.Dispose();
            }
        }
    }
}