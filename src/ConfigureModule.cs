﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using Autofac;
using CommandLine;
using MediatR;

namespace KomzdravContentExporter
{
    public class ConfigureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof (IMediator).Assembly).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(ThisAssembly).AsImplementedInterfaces();
            builder.Register<SingleInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            builder.Register<MultiInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => (IEnumerable<object>) c.Resolve(typeof (IEnumerable<>).MakeGenericType(t));
            });
            
            var parser = new Parser(settings =>
            {
                settings.CaseSensitive = false;
                settings.EnableDashDash = true;
                settings.HelpWriter = Console.Error;
            });
            builder.RegisterInstance(parser).AsSelf();
            builder.RegisterInstance(new ConsoleProgress()).AsImplementedInterfaces().SingleInstance();
        }
    }
}