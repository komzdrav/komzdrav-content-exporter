using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace KomzdravContentExporter.Options.Handlers
{
    public abstract class RequestVerbHandler<TVerb> : IRequestHandler<TVerb, Unit> where TVerb : Option
    {
        private TVerb _option;

        protected RequestVerbHandler(IProgress<string> progress)
        {
            _progress = progress;
        }

        public Unit Handle(TVerb option)
        {
            _option = option;
            _progress.Report("Extracting data, please wait ");
            HandleCore(option);
            return Unit.Value;
        }

        private IProgress<string> _progress;

        protected abstract void HandleCore(TVerb option);

        protected string Save(string name, object value)
        {
            var filePath = Path.Combine(_option.OutputPath, Path.HasExtension(name) ? name : $"{name}.json");

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            Directory.CreateDirectory(Path.GetDirectoryName(filePath) ?? _option.OutputPath);
            using (var outputStream = File.OpenWrite(filePath))
            using (var streamWriter = new StreamWriter(outputStream))
            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                jsonWriter.Formatting = Formatting.Indented;

                var serializer = new JsonSerializer();

                serializer.FloatParseHandling = FloatParseHandling.Decimal;
                serializer.FloatFormatHandling = FloatFormatHandling.DefaultValue;
                
                serializer.Converters.Add(new IsoDateTimeConverter());
                serializer.Converters.Add(new StringEnumConverter());
                serializer.ContractResolver = new CamelCasePropertyNamesContractResolver();
                serializer.Serialize(jsonWriter, value);
            }

            return filePath;
        }

        protected TResult WithConnection<TResult>(Func<IDbConnection, TResult> selector)
        {
            using (var connection = new SqlConnection(_option.ConnectionString))
            {
                connection.Open();
                return selector(connection);
            }
        }
    }
}