﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using Dapper;
using ImageProcessor;
using ImageProcessor.Imaging.Formats;

namespace KomzdravContentExporter.Options.Handlers
{
    public class OrganizationVerbHandler : RequestVerbHandler<OrganizationVerb>
    {
        protected override void HandleCore(OrganizationVerb option)
        {
            //1. get organization info from database
            foreach (var record in GetOrganizations())
            {
                //2. get organization contacts from database
                //3. map organization (type)
                //4. download organization images
                //5. save images on disk
                //6. serialize and save on disk organization
                Save(record.Name.Replace(".", string.Empty),
                    new Organization(record, GetOrganizationContacts(record.Id),
                        DownloadOrganizationImage(record.Images, option.Url, option.OutputPath)));
            }
        }

        private IEnumerable<string> DownloadOrganizationImage(string images, string baseUrl, string outputPath)
        {
            if (string.IsNullOrEmpty(images))
            {
                yield break;
            }

            var imageName = images.Split(';').OrderBy(x => x).First();
            var httpClient = new HttpClient()
            {
                BaseAddress = new Uri(new Uri(baseUrl), new Uri("/Content/Images/Clinics/", UriKind.Relative))
            };
            var response = httpClient.GetAsync(imageName).Result;

            if (response.IsSuccessStatusCode == false)
            {
                yield break;
            }

            var imageInputStream = response.Content.ReadAsStreamAsync().Result;

            using (var outputStream = OpenWrite(Path.Combine(outputPath, imageName)))
            {
                imageInputStream.Position = 0;
                using (var imageFactory = new ImageFactory())
                {
                    //resize
                    imageFactory
                        .Load(imageInputStream)
                        .Resize(new Size(181, 119))
                        .Format(new JpegFormat())
                        .Save(outputStream);
                    //5. save image on disk
                    yield return imageName;
                }
            }
        }

        private Stream OpenWrite(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            return File.OpenWrite(filePath);
        }

        private IEnumerable<OrganizationRecord> GetOrganizations()
        {
            string sql = @"SELECT c.Name,
                                   c.ShortName AS ALIAS,
                                   c.city,
                                   c.Street,
                                   c.PostalCode,
                                   c.Lat,
                                   c.Lng,
                                   c.Link,
                                   c.Images,
                                   c.Type,
                                   c.TalonLink as AppointmentLink,
                                   c.PaymentServiceLink as PaidServicesLink,
                                   c.Id,
                                   c.District
                            FROM Clinic c;";

            return WithConnection(conn => conn.Query<OrganizationRecord>(sql));
        }

        private IEnumerable<OrganizationContackRecord> GetOrganizationContacts(int id)
        {
            string sql = @"SELECT distinct *
                            FROM OrganizationContacts oc
                            WHERE oc.OrganizationId = @id;";
            return WithConnection(conn => conn.Query<OrganizationContackRecord>(sql, new {id}));
        }

        public OrganizationVerbHandler(IProgress<string> progress) : base(progress)
        {
        }
    }

    internal class OrganizationContackRecord
    {
        public string Type { get; set; }
        public string Value { get; set; }
        public string Category { get; set; }
    }

    internal class OrganizationRecord
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public string Link { get; set; }
        public string Images { get; set; }
        public int Type { get; set; }
        public string AppointmentLink { get; set; }
        public string PaidServicesLink { get; set; }
        public int District { get; set; }
        
    }

    internal class Organization
    {
        private OrganizationRecord _organizationRecord;

        public Organization(OrganizationRecord organizationRecord, IEnumerable<OrganizationContackRecord> contackRecords,
            IEnumerable<string> images)
        {
            _organizationRecord = organizationRecord;
            Address = new Address(organizationRecord.Street);
            Telecom = new List<ContactPoint>(new List<ContactPoint>
            {
                {new ContactPoint("Заказ талона", "Url", ToAbsoluteUrl(_organizationRecord.AppointmentLink))},
                {new ContactPoint("Платные услуги", "Url", ToAbsoluteUrl(_organizationRecord.PaidServicesLink))},
            }.Concat(contackRecords.Select(cr => new ContactPoint(cr.Category, cr.Type, cr.Value))));
            Images = new List<string>(images);
        }

        public int Type => _organizationRecord.Type;
        public string Name => _organizationRecord.Name;
        public string Alias => _organizationRecord.Alias;
        public IEnumerable<ContactPoint> Telecom { get; set; }
        public Address Address { get; set; }
        public District District => (District) _organizationRecord.District;
        public decimal Latitude => _organizationRecord.Lat;
        public decimal Longitude => _organizationRecord.Lng;
        public IEnumerable<string> Images { get; set; }


        private string ToAbsoluteUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return string.Empty;
            }

            try
            {
                UriBuilder builder = new UriBuilder(url);
                return builder.Uri.AbsoluteUri;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }

    internal class Address
    {
        public Address(string street)
        {
            Text = street;
        }

        public string Text { get; set; }
    }

    internal class ContactPoint
    {
        public ContactPoint(string purpose, string system, string value)
        {
            Purpose = purpose;
            System = system;
            Value = value;
        }

        public string Purpose { get; set; }
        public string System { get; set; }
        public string Value { get; set; }
    }


    public enum District
    {
        [EnumMember(Value = "")] Minsk,

        [EnumMember(Value = "Центральный")] Tsentralnyi,

        [EnumMember(Value = "Советский")] Sovetskii,

        [EnumMember(Value = "Первомайский")] Pervomaiskii,

        [EnumMember(Value = "Партизанский")] Partizanskii,

        [EnumMember(Value = "Заводской")] Zavodskoi,

        [EnumMember(Value = "Ленинский")] Leninskii,

        [EnumMember(Value = "Октябрьский")] Oktyabrskii,

        [EnumMember(Value = "Московский")] Moskovskii,

        [EnumMember(Value = "Фрунзенский")] Frunzenskii
    }
}