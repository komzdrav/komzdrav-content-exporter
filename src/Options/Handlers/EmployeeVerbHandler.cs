﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dapper;

namespace KomzdravContentExporter.Options.Handlers
{
    public class EmployeeVerbHandler : RequestVerbHandler<EmployeeVerb>
    {
        protected override void HandleCore(EmployeeVerb message)
        {
            //1. get employees from db
            foreach (var employeeRecord in GetEmployees())
            {
                var encounters = GetEmployeeEncounters(employeeRecord.Id);
                var activity = GetEmployeeActivityes(employeeRecord.Id);

                //2. map to employee model
                var employee = new Employee(employeeRecord, encounters, activity);
            

                //3. serialize
                //4. save to output 
                Save(employeeRecord.Name, employee);
            }
        }


        private IEnumerable<EmployeeRecord> GetEmployees()
        {
            string sql =
                "select empl.Id, empl.Name, empl.Position, empl.[Order] as Rank from Persons as empl order by Rank;";
            return WithConnection(conn => conn.Query<EmployeeRecord>(sql));
        }

        private IEnumerable<EmployeeEncounterRecord> GetEmployeeEncounters(Guid employeeId)
        {
            string sql =
                @"select ev.EventType as [type], ev.Phone as telecom, evs.[DayOfWeek], evs.Start, evs.[End],  ev.Location, ev.Address, ev.Topic ,evs.ExceptionString as exception
                      from [Event] ev
	                    join EventSchedule evs on evs.EventId = ev.Id
                     where ev.PersonId = @employeeId";
            return WithConnection(conn => conn.Query<EmployeeEncounterRecord>(sql, new {employeeId}));
        }


        private IEnumerable<string> GetEmployeeActivityes(Guid employeeId)
        {
            string sql =
                @"select pe.Activity  as activity
                      from [PersonActivity] pe
                     where pe.PersonId = @employeeId";
            return WithConnection(conn => conn.Query<string>(sql, new { employeeId }));
        }

        public EmployeeVerbHandler(IProgress<string> progress) : base(progress)
        {
        }
    }

    internal class Employee
    {
        private readonly EmployeeRecord _employeeRecord;
        private readonly IEnumerable<EmployeeEncounterRecord> _employeeEncounterRecords;
        private readonly IEnumerable<string> _employeeActivity;


        public Employee(EmployeeRecord employeeRecord, IEnumerable<EmployeeEncounterRecord> employeeEncounterRecords, IEnumerable<string> employeeActivity)
        {
            _employeeRecord = employeeRecord;
            _employeeEncounterRecords = employeeEncounterRecords;
            _employeeActivity = employeeActivity;
        }

        public string Name => _employeeRecord.Name;

        public string Position => _employeeRecord.Position;
        public int Rank => _employeeRecord.Rank;
        public IEnumerable<Encounter> Encounters => _employeeEncounterRecords.Select(e => new Encounter(e)).ToList();
        public IEnumerable<string> Activity => _employeeActivity;
    }

    internal class Encounter
    {
        private readonly EmployeeEncounterRecord _employeeEncounterRecord;

        public Encounter(EmployeeEncounterRecord employeeEncounterRecord)
        {
            _employeeEncounterRecord = employeeEncounterRecord;
        }

        public string Type => _employeeEncounterRecord.Type;
        public string Telecom => _employeeEncounterRecord.Telecom;
        public string Location => _employeeEncounterRecord.Location;
        public string Topic => _employeeEncounterRecord.Topic;
        public string Address => _employeeEncounterRecord.Address;

        public DayOfWeek DayOfWeek
            =>
                _employeeEncounterRecord.Type.Equals("personal")
                    ? ConvertToSystemDayOfWeek(_employeeEncounterRecord.DayOfWeek.Trim())
                    : Start.DayOfWeek;

        public DateTime Start => GetEventDate(_employeeEncounterRecord.Start);

        public DateTime End => GetEventDate(_employeeEncounterRecord.End);

        public string Exception => _employeeEncounterRecord.Exception?.Trim();

        private DateTime GetEventDate(DateTime dateTime)
        {
            DateTime eventDate = DateTime.MinValue;


            if (_employeeEncounterRecord.Type.Equals("virtual", StringComparison.InvariantCultureIgnoreCase)
                || _employeeEncounterRecord.Type.Equals("field", StringComparison.InvariantCultureIgnoreCase))
            {
                eventDate = DateTime.ParseExact(_employeeEncounterRecord.DayOfWeek.Trim(), "dd.MM.yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None);
            }


            return new DateTime(eventDate.Year, eventDate.Month, eventDate.Day, dateTime.Hour, dateTime.Minute, 00);
        }

        private DayOfWeek ConvertToSystemDayOfWeek(string dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case "воскресенье":
                    return DayOfWeek.Sunday;
                case "понедельник":
                    return DayOfWeek.Monday;
                case "вторник":
                    return DayOfWeek.Tuesday;
                case "среда":
                    return DayOfWeek.Wednesday;
                case "четверг":
                    return DayOfWeek.Thursday;
                case "пятница":
                    return DayOfWeek.Friday;
                case "суббота":
                    return DayOfWeek.Saturday;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dayOfWeek));
            }
        }
    }

    internal class EmployeeEncounterRecord
    {
        public string Type { get; set; }
        public string Telecom { get; set; }
        public string DayOfWeek { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string Topic { get; set; }
        public string Exception { get; set; }
    }

    internal class EmployeeRecord
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public int Rank { get; set; }
    }
}