﻿using System;
using System.Collections.Generic;
using Dapper;
using Newtonsoft.Json;

namespace KomzdravContentExporter.Options.Handlers
{
    public class FaqVerbHandler : RequestVerbHandler<FaqVerb>
    {
        protected override void HandleCore(FaqVerb option)
        {
            //1. get faq from database
            foreach (var record in GetQuestions())
            {
                //2. serialize and sve on disk
                Save($"question_{record.Id}", record);
            }
        }

        private IEnumerable<FaqRecord> GetQuestions()
        {
            string sql = "select ROW_NUMBER() over(order by FaqGroup) as id , Question, Answer, FaqGroup as category from Faq;";
            return WithConnection(conn => conn.Query<FaqRecord>(sql));
        }

        public FaqVerbHandler(IProgress<string> progress) : base(progress)
        {
        }
    }

    internal class FaqRecord
    {
        [JsonIgnore]
        public int Id { get; set; }

        public string Question { get; set; }
        public string Answer { get; set; }
        public string Category { get; set; }
    }
}