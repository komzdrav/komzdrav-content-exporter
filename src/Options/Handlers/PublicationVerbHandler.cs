﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using Dapper;
using ImageProcessor;
using ImageProcessor.Imaging.Formats;
using NickBuhro.Translit;
using ReverseMarkdown;

namespace KomzdravContentExporter.Options.Handlers
{
    public class PublicationVerbHandler : RequestVerbHandler<PublicationVerb>
    {
        public PublicationVerbHandler(IProgress<string> progress) : base(progress)
        {
        }

        protected override void HandleCore(PublicationVerb option)
        {
            //1. get publication
            foreach (var record in GetPublicationSince(option.Since))
            {
                //2. celan from html
                //2.1 annotation
                //2.2 title
                var title = RemoveTags(record.Title);
                var annotation = RemoveTags(record.Annotation);


                var publicationImages = new List<string>();
                //3. extract image links from body
                foreach (var uri in GetImageLinksFromContent(record.Content, option.Url))
                    try
                    {
                        var httpClient = new HttpClient();
                        var response = httpClient.GetAsync(uri).Result;

                        if (!response.IsSuccessStatusCode) continue;

                        //4. download image
                        var imageContent = response.Content.ReadAsStreamAsync().Result;
                        using (SHA1 sha = new SHA1Managed())
                        {
                            string fileName =
                                $"{BitConverter.ToString(sha.ComputeHash(imageContent)).Replace("-", string.Empty)}.jpg";
                            using (var outputStream = OpenWrite(Path.Combine(option.OutputPath, fileName)))
                            {
                                imageContent.Position = 0;
                                using (var imageFactory = new ImageFactory())
                                {
                                    //resize
                                    imageFactory
                                        .Load(imageContent)
                                        .Resize(new Size(800, 530)).BackgroundColor(ColorTranslator.FromHtml("#e5eef9"))
                                        .Format(new JpegFormat())
                                        .Save(outputStream);
                                    //5. save image on disk
                                    publicationImages.Add(fileName);
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                //6. convert body html to md
                var content = new Converter().Convert(RemoveImgFromHtml(record.Content));

                var slug = Transliteration.CyrillicToLatin(title).Replace(" ", "-").Replace(".", string.Empty);
                slug = Path.GetInvalidFileNameChars()
                    .Aggregate(slug, (current, invalidFileNameChar) => current.Replace(invalidFileNameChar, ' '));
                //7. serialize and save on disk
                var fileInfo = new FileInfo(Save(slug, new Publication(title, annotation, content, publicationImages)));
                //8. set published date as file creation time
                fileInfo.CreationTime = record.PublicationDate;
            }
        }

        private string RemoveTags(string html)
        {
            return Regex.Replace(html, "<.*?>", string.Empty);
        }

        public string RemoveImgFromHtml(string html)
        {
            return Regex.Replace(html, @"<img\s[^>]*>(?:\s*?</img>)?", string.Empty);
        }

        private Stream OpenWrite(string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            return File.OpenWrite(filePath);
        }

        private IEnumerable<PublicationRecord> GetPublicationSince(DateTime since)
        {
            var sql = @"SELECT p.Title,
                                   p.Annotation,
                                   p.Content,
                                   p.PublicationDate
                            FROM Publication p
                            WHERE p.Type = 1
                              AND p.PublicationDate >= @since
                            ORDER BY p.PublicationDate DESC";
            return WithConnection(conn => conn.Query<PublicationRecord>(sql, new {since}));
        }

        private IEnumerable<Uri> GetImageLinksFromContent(string content, string host)
        {
            foreach (
                Match match in
                Regex.Matches(content, @"<img\b[^\<\>]+?\bsrc\s*=\s*[""'](?<L>.+?)[""'][^\<\>]?\>",
                    RegexOptions.IgnoreCase | RegexOptions.Multiline))
            {
                Uri relative;
                var uriString = match.Groups[1].Value.Split(new[] {" ", "\""}, StringSplitOptions.None)[0];
                if (Uri.TryCreate(uriString, UriKind.Relative, out relative))
                    yield return new Uri(new Uri(host), relative);
                else
                    yield return new Uri(uriString, UriKind.Absolute);
            }
        }
    }

    internal class PublicationRecord
    {
        public string Title { get; set; }
        public string Annotation { get; set; }
        public string Content { get; set; }
        public DateTime PublicationDate { get; set; }
    }

    public class Publication
    {
        public Publication(string title, string annotation, string content, IEnumerable<string> images)
        {
            Title = title;
            Annotation = annotation;
            Content = content;
            Images = images;
        }

        public string Title { get; set; }
        public string Annotation { get; set; }
        public string Content { get; set; }
        public IEnumerable<string> Images { get; set; }
    }
}