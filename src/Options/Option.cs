using System;
using System.Configuration;
using CommandLine;
using MediatR;

namespace KomzdravContentExporter.Options
{
    public abstract class Option : IRequest
    {
        [Option('c', "conn", HelpText = "The connection string to server and database you want to extract")]
        public string ConnectionString { get; set; }

        [Option('o', "output", HelpText = "Output path")]
        public string OutputPath { get; set; }

        [Option("url", HelpText = "URL to website", Default = "http://komzdrav-minsk.gov.by/")]
        public string Url { get; set; }
        

        protected Option()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["default"]?.ConnectionString;
            OutputPath = Environment.CurrentDirectory;
        }
    }
}