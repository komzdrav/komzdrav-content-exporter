using System;
using CommandLine;

namespace KomzdravContentExporter.Options
{
    [Verb("news", HelpText = "Extract publication and news")]
    public class PublicationVerb : Option
    {
        [Option('s', "since", HelpText = "since date to present")]
        public DateTime Since { get; set; }

        public PublicationVerb()
        {
            Since = DateTime.Today.AddYears(-1);
        }
    }
}