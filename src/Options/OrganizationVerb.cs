using System;
using System.Data;
using CommandLine;
using KomzdravContentExporter.Options;

namespace KomzdravContentExporter
{
    [Verb("organization", HelpText = "Extract contact information about healthcare organizations")]
    public class OrganizationVerb : Option
    {
    }
}