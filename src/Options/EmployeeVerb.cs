using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using Autofac;
using CommandLine;
using Xunit;

namespace KomzdravContentExporter.Options
{
    [Verb("employee", HelpText = "Extract information about employees")]
    public sealed class EmployeeVerb : Option
    {
    }

    public class EmployeeVerbTests
    {
        private Parser _parser;

        public EmployeeVerbTests()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
            var container = builder.Build();
            _parser = container.Resolve<Parser>();
        }


        [Fact]
        public void If_there_are_no_options_passed_should_be_initialized_with_defaults_params()
        {
            var sut = _parser.ParseArguments<EmployeeVerb>(new[] {"employee"})
                .MapResult(option => option, errors => null);

            Assert.NotNull(sut);
            Assert.Equal(Environment.CurrentDirectory, sut.OutputPath);
            Assert.Equal(ConfigurationManager.ConnectionStrings["default"]?.ConnectionString, sut.ConnectionString);
        }

        [Fact]
        public void Passed_options_should_override_defaults()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["default"]?.ConnectionString ?? string.Empty;
            var outputPath = Path.GetTempPath();

            var sut = _parser.ParseArguments<EmployeeVerb>(new[]
            {
                "employee",
                "-c", connectionString,
                "-o", outputPath
            })
                .MapResult(option => option, errors => null);

            Assert.NotNull(sut);
            Assert.Equal(connectionString, sut.ConnectionString);
            Assert.Equal(outputPath, sut.OutputPath);
        }
    }
}